#include "Menu.h"



Menu::Menu()
{
}


Menu::~Menu()
{
}

void Menu::menuPanel()
{
	int choice = 0;
	do
	{
		bool finish = false;
		std::cout << "What type of data structure do you want to create?\n";
		std::cout << "1. Table..." << std::endl;
		std::cout << "2. Quit" << std::endl;
		do
		{
			std::cout << "Your choice: ";
			std::cin >> choice;
			std::cin.get();
			if (choice > 2 || choice <= 0)
			{
				std::cout << "Incorrect choice, try again.\n";
			}
		} while (choice > 2 || choice <= 0);
		std::cout << "\n";

		switch (choice)
		{
		case 1:
			tableMethods();
			break;
		default:
			break;
		}
	} while (choice != 2);
}

void Menu::tableMethods()
{
	int choice = 0;
	int element = 0;
	int position = 0;
	int number = 0;
	int rangeU = 0;
	int rangeD = 0;
	std::string filename = "";

	do
	{
		//->Table...
		std::cout << "What do you want to do with table?" << std::endl;
		std::cout << "1. Show actual table" << std::endl;
		std::cout << "2. Add new element..." << std::endl;
		std::cout << "3. Delete element..." << std::endl;
		std::cout << "4. Find element..." << std::endl;
		std::cout << "5. Fill table from file..." << std::endl;
		std::cout << "6. Save to file" << std::endl;
		std::cout << "7. Add N random numbers..." << std::endl;
		std::cout << "8. Back" << std::endl;

		do
		{
			std::cout << "Your choice: ";
			std::cin >> choice;
			std::cin.get();
			if (choice > 8 || choice <= 0)
			{
				std::cout << "Incorrect choice, try again." << std::endl;
			}
		} while (choice > 8 || choice <= 0);
		std::cout << std::endl;

		switch (choice)
		{
		case 1:
			table.showTable();
			break;

		case 2:
			std::cout << "Value of new element: ";
			std::cin >> element;
			std::cin.get();

			std::cout << "Index of new element (0 - " << table.getSize() << "): ";
			std::cin >> position;
			std::cin.get();

			table.addElementToTable(position, element);
			break;

		case 3:
			if (table.getSize() != 0)
			{
				std::cout << "Index of element to delete (0 - " << table.getSize() - 1 << "): ";
				std::cin >> position;
				std::cin.get();
				table.deleteElementFromTable(position);
			}
			else
			{
				std::cout << "[INFO] Table is empty, nothing to delete." << std::endl << std::endl;
			}
			break;

		case 4:
			
			std::cout << "Find first occurence of value: ";
			std::cin >> element;
			std::cin.get();

			position = table.findElementInTable(element);
			//-1 is default, it just means it isn't real position
			if (position == -1)
			{
				std::cout << "Value was not found in this table." << std::endl << std::endl;
			}
			else if (position >= 0)
			{
				std::cout << "Index of found element is: " << position << std::endl << std::endl;
			}
			break;

		case 5:
			std::cout << "Insert file's name (without extension): ";
			std::cin >> filename;
			std::cin.get();

			table.fillFromFile(filename);
			break;
		case 6:
			std::cout << "Insert file's name (without extension): ";
			std::cin >> filename;
			std::cin.get();

			table.saveToFile(filename);
			break;
		case 7:
			std::cout << "Number of elements to add: ";
			std::cin >> number;
			std::cin.get();
			
			std::cout << "Bottom range of randoms: ";
			std::cin >> rangeD;
			std::cin.get();

			std::cout << "Upper range of randoms: ";
			std::cin >> rangeU;
			std::cin.get();

			table.addNRandomElementsToTable(number, rangeD, rangeU);
			break;
		default:
			break;
		}
	} while (choice != 8);
	

}

