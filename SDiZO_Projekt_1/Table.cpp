#include "Table.h"



Table::Table()
{
	this->size = 0;
	this->table = new int[size];
}

Table::~Table()
{
	delete[] this->table;
}

int* Table::getTable()
{
	return this->table;
}

int Table::getSize()
{
	return this->size;
}

/*void Table::createTable(int size)
{
	this->table = new int[size];
	this->size = 0;
}

void Table::deleteTable()
{
	delete []table;
	this->size = 0;
}

void Table::setTable(int * table, int size)
{
	this->table = table;
	this->size = size;
}*/

//position is index of place where you want to put new element
void Table::addElementToTable(int position, int newElement)
{
	//adding element may happen from pos. 0 to size (as new last element)
	if (position <= size && position >= 0)
	{
		int* tableTemp = new int[size + 1];
		for (int i = 0; i < position; i++)
		{
			tableTemp[i] = table[i];
		}

		tableTemp[position] = newElement;

		for (int i = position; i < size; i++)
		{
			tableTemp[i + 1] = table[i];
		}
		delete []table;
		this->table = tableTemp;
		this->size++;
		std::cout << "[INFO] Adding to table completed." << std::endl;
	}
	else
	{
		std::cout << "[INFO] Position is out of table, element has NOT been added." << std::endl << std::endl;
	}
		
}

//position is index of element you want to delete
void Table::deleteElementFromTable(int position)
{
	if (position >= 0 && position <= size - 1)
	{
		int* tableTemp = new int[size - 1];
		for(int i = 0 ; i < position; i++)
		{
			tableTemp[i] = table[i];
		}
		for(int i = position + 1; i < size; i++)
		{
			tableTemp[i - 1] = table[i];
		}

		delete []table;
		this->table = tableTemp;
		this->size--;
		std::cout << "[INFO] Deleting from table completed." << std::endl << std::endl;
	}
	else
	{
		std::cout << "[INFO] Position is out of table, element has NOT been deleted." << std::endl << std::endl;
	}
}

int Table::findElementInTable(int value)
{
	//-1 if didn't find value
	int positionOfFoundElement = -1;

	for(int i = 0 ; i < size; i++)
	{
		if (table[i] == value)
		{
			std::cout << "[INFO] Value found in table.";
			positionOfFoundElement = i;
			break;
		}
	}

	return positionOfFoundElement;
}

void Table::fillFromFile(std::string filename)
{
	filename += ".txt";
	std::ifstream read;
	read.open(filename);
	if (read.is_open())
	{
		std::cout << "[INFO] File has been opened." << std::endl;
		size = 0;
		table = new int[size];
		int value = 0;

		while (!read.eof())
		{
			read >> value;
			addElementToTable(size,value);
		}
		std::cout << "[INFO] Filling table finished." << std::endl;
	}
	else
	{
		std::cout << "[INFO] File has NOT been opened. Table is not filled." << std::endl;
	}
	read.close();
}

void Table::saveToFile(std::string filename)
{
	filename += ".txt";
	std::ofstream write;
	write.open(filename);
	if (write.is_open())
	{
		for (int i = 0 ; i < size ; i++)
		{
			write << table[i] << std::endl;
		}
		std::cout << "[INFO] Table saved to file." << std::endl;
	}
	else
	{
		std::cout << "[INFO] File could NOT be created/opened. " << std::endl;
	}
}

void Table::addNRandomElementsToTable(int numberOfNewElements, int rangeDown, int rangeUp)
{
	for (int i = 0 ; i < numberOfNewElements; i++)
	{
		addElementToTable(size,rand() % rangeUp + rangeDown);
	}
	std::cout << "[INFO] " << numberOfNewElements << " numbers added." << std::endl;
}

void Table::showTable()
{
	if (size != 0)
	{
		std::cout << "_____Table_____" << std::endl;
		for (int i = 0; i < size; i++)
		{
			std::cout << table[i] << std::endl;
		}
		std::cout << "_______________" << std::endl;
	}
	else
	{
		std::cout << "Table is empty." << std::endl;
	}
}
