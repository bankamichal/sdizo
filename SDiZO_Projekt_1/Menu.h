#pragma once
#include "Libraries.h"
#include "Table.h"

class Menu
{
private:
	Table table;
public:
	Menu();
	~Menu();

	void menuPanel();
	void tableMethods();
};

