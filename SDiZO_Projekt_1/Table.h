#pragma once
#include "Libraries.h"

class Table
{
private:
	int* table;
	int size;
public:
	Table();
	~Table();

	int* getTable();
	int getSize();

	/*//use these only if it's really needed
	void createTable(int size);
	void deleteTable();
	void setTable(int* table, int size);*/

	void addElementToTable(int position, int newElement);
	void deleteElementFromTable(int position);
	int findElementInTable(int value);
	void fillFromFile(std::string filename);
	void saveToFile(std::string filename);
	void addNRandomElementsToTable(int numberOfNewElements, int rangeDown, int rangeUp);


	void showTable();
};

